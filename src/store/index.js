import Vue from 'vue'
import Vuex from 'vuex'

import {state, getters} from './state'
import mutations from './mutations'
import actions from './actions'

Vue.use(Vuex);

export default new Vuex.Store({
    state: state,
    mutations: mutations,
    actions: actions,
    getters: getters
})