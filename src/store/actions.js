import {$key} from '../main'

class Weather {
    constructor(info) {
        this.id = info.id;
        this.name = info.name;
        this.country = info.sys.country;
        this.temp = Math.round(info.main.temp);
        this.weather = info.weather[0].main;
        this.icon = info.weather[0].icon;
        this.pressure = info.main.pressure;
        this.humidity = info.main.humidity;
        this.wind = info.wind.speed;
    }
}

class City {
    constructor(info, defaultID) {
        this.id = info.id;
        this.name = info.name;
        this.default = this.id === defaultID
    }
}

const sortByField = (field) => (a, b) => a[field] > b[field] ? 1 : -1;

export default {
    getDefaultWeatherInfo: async function ({dispatch, commit}, location) {
        try {
            let {latitude, longitude} = location,
                api = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${$key}&units=metric`);

            let info = await api.json(),
                weather = new Weather(info);

            commit('setDefaultWeatherInfo', weather);
            dispatch('saveCurrentWeatherToUserList', weather);

        } catch (err) {
            console.warn(err)
        }

    },

    deleteDefaultWeatherInfo: function ({dispatch, state, commit}) {
        let userCities = state.userSavedCitiesList;

        for (let i = 0; i < userCities.length; i++) {
            if (userCities[i].default === true) {
                userCities.splice(i, 1);
                break;
            }
        }

        commit('saveCityToList', userCities);
        dispatch('slidingWeather');
    },

    findCurrentWeatherOfCity: async function ({commit}, payload) {
        try {

            let {city, isNew} = payload;

            let api = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${$key}&units=metric`);
            let info = await api.json(),
                weather = new Weather(info);

            if (isNew) {
                commit('setCurrentWeatherOfCity', weather);
            }

            return weather;
        } catch (err) {
            return false
        }
    },

    saveCurrentWeatherToUserList: function ({dispatch, state, commit}, weather) {
        let userCities = state.userSavedCitiesList,
            same = false,
            defaultID = state.defaultWeather.id ? state.defaultWeather.id : 0;

        for (let i = 0; i < userCities.length; i++) {
            if (userCities[i].id === weather.id) {
                same = true;
                break;
            }
        }

        if (!same) {
            let city = new City(weather, defaultID);
            userCities.push(city);
            userCities.sort(sortByField('name'));
            commit('saveCityToList', userCities);
            commit('setCurrentWeatherOfCity', {});
            dispatch('slidingWeather');
            return true
        } else {
            return false
        }
    },

    deleteCityFromUserList: function ({dispatch, state, commit}, id) {
        let userCities = state.userSavedCitiesList;

        for (let i = 0; i < userCities.length; i++) {
            if (userCities[i].id === id) {
                userCities.splice(i, 1);
                break;
            }
        }

        commit('saveCityToList', userCities);
        dispatch('slidingWeather');
        return true;
    },

    slidingWeather: async function ({state, commit}) {
        let userCities = state.userSavedCitiesList,
            citiesIDs = userCities.map(city => city.id).join(',');

        let weatherArray = [];

        if (userCities.length > 0) {
            let api = await fetch(`https://api.openweathermap.org/data/2.5/group?id=${citiesIDs}&units=metric&appid=${$key}`),
                data = (await api.json()).list;

            for (let i = 0; i < data.length; i++) {
                weatherArray.push(new Weather(data[i]))
            }
        }

        commit('setSlidingWeatherArray', weatherArray);
    }
}