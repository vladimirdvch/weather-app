export {state, getters};

const loadUserCitiesFromStorage = () => {
    try {
        let data = localStorage.getItem('cities-list'),
            list = JSON.parse(data);

        if (list === null) return undefined;
        else return list;
    } catch (err) {
        localStorage.removeItem('cities-list');
    }
};

const state = {
    defaultWeather: {},
    currentWeather: {},
    userSavedCitiesList: loadUserCitiesFromStorage() || [],
    slidingWeatherArray: []
};

const getters = {
    defaultWeather: state => state.defaultWeather,
    geolocationStatus: state => Object.keys(state.defaultWeather).length > 0,
    currentWeather: state => state.currentWeather,
    userSavedCitiesList: state => state.userSavedCitiesList,
    slidingWeatherArray: state => state.slidingWeatherArray
};