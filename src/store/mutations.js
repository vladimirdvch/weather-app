const saveUserCitiesToStorage = (list) => {
    try {
        let data = JSON.stringify(list);
        localStorage.setItem('cities-list', data);
    } catch (err) {
        console.warn(err)
    }

};

export default {
    setDefaultWeatherInfo: function (state, payload) {
        state.defaultWeather = payload
    },
    setCurrentWeatherOfCity: function (state, payload) {
        state.currentWeather = payload
    },
    clearCurrentWeather: function (state) {
        state.currentWeather = {}
    },
    saveCityToList: function (state, payload) {
        state.userSavedCitiesList = payload;
        if (state.userSavedCitiesList.length > 0) {
            saveUserCitiesToStorage(state.userSavedCitiesList);
        } else {
            localStorage.removeItem('cities-list');
        }
    },
    setSlidingWeatherArray: function (state, payload) {
        state.slidingWeatherArray = payload
    }
}