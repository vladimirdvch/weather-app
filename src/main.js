import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'

export {$key}

import 'materialize-css/dist/js/materialize.min';
import 'materialize-css/dist/css/materialize.min.css';
Vue.prototype.$M = window.M;

Vue.config.productionTip = false;
Vue.prototype.$key = process.env.NODE_ENV === 'production' ? process.env.WEATHER_KEY : '8b96b14f08ecd9c1624a59b62b8fbfbe';
const $key = Vue.prototype.$key;

export const eventEmitter = new Vue();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
